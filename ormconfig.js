console.log("NODE_ENV", process.env.NODE_ENV)

const getConfig = (env) => envs[env] || envs['development']

const cli = {
  "migrationsDir": `./${process.env.NODE_ENV === 'development' ? 'src' : 'dist'}/database/migrations`
}

const envs = {
  production: {
    "name": "survey_email-production",
    
    "url": process.env.DATABASE_URL,
    "type": "postgres",
    // "host": "ec2-54-161-239-198.compute-1.amazonaws.com", 
    // "user": "mcqbtioalotjbj",
    // "port": "5432",
    // "password": "1b97700efabf702bea7e0258f66d162bb3edf1d0f217b479d654c5ca8983d737",
    // "database": "d89r6hr0duljfj",
  
    "migrationsRun": true,
    "synchronize": true,
    "logging": false,
    "extra": {
      "ssl": true,
    },
  
    "migrations": ["./dist/database/migrations/**.js"],
    "entities": ["./dist/models/**.js"],
    cli,
  },

  ["production-dev"]: {
    "type": "postgres",
    // "url": 'postgres://test:test@localhost:3333/dist/database/database.pg',
    "host": "localhost", 
    // "port": "3333",
    // "port": "5432",
    "port":  54320,
    "user": "test",
    "password": "test",
    // "database": "survey_email-production-dev.db",
    "databse": "my_postgres",
  
    // "migrationsRun": true,
    "synchronize": true,
    "logging": false,

    // "ssl": {
    //   "rejectUnauthorized": false,
    // },

    // "migrations": ["./dist/database/migrations/**.js"],
    // "entities": ["./dist/models/**.js"],
    // cli,
  },

  development: {
    "type": "sqlite",
    "database": "./src/database/database.sqlite",
    "migrations": ["./src/database/migrations/**.ts"],
    "entities": ["./src/models/**.ts"],
    cli,
  }
}

console.log(getConfig(process.env.NODE_ENV))
module.exports = getConfig(process.env.NODE_ENV)