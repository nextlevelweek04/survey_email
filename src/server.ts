import dotenv from 'dotenv'
import { app } from './app'

dotenv.config()

app.listen(3333, () => {
  console.log("Server Running!")
})

/*

Melhorias:

1- Validação das requests com (Joy ou outra dependcia)

*/
