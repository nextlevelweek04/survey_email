import { Request, Response } from 'express'
import { getCustomRepository } from "typeorm"
import { SurveysUsersRepository } from "../repositories/SurveysUsersRepository"
import { AppError } from '../errors/AppError'

class AnswerController {

  async execute(req: Request, resp: Response) {
    const { value } = req.params
    const { u } = req.query

    const surveysUsersRepository = getCustomRepository(SurveysUsersRepository)

    const surveyUser = await surveysUsersRepository.findOne({
      id: String(u)
    })

    if(!surveyUser) throw new AppError(`Survey User '${u}' does not exists!`)

    surveyUser.value = Number(value)

    await surveysUsersRepository.save(surveyUser)

    return resp.json(surveyUser)
  }

}

export { AnswerController }