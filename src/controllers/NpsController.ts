import { Request, Response } from 'express'
import { getCustomRepository, Not, IsNull } from 'typeorm'
import { SurveysUsersRepository } from '../repositories/SurveysUsersRepository'

class NPSController {

  async execute(req: Request, resp: Response) {
    const { survey_id } = req.params

    const surveysUsersRepository = getCustomRepository(SurveysUsersRepository)

    const surveysUsers = await surveysUsersRepository.find({
      survey_id,
      value: Not(IsNull()),
    })

    const totalAnswers = surveysUsers.length

    const detractors = surveysUsers.filter(s => s.value <= 6).length
    const promoters = surveysUsers.filter(s => s.value >= 9).length
    const passives = [totalAnswers, detractors, promoters].reduce((total, current) => total - current)

    const calculate = (( (promoters - detractors) / totalAnswers ) * 100).toFixed(2)

    return resp.json({
      totalAnswers,
      detractors,
      promoters,
      passives,
      nps: Number(calculate),
    })

  }

}

export { NPSController }