import { Request, Response } from 'express'
import { getCustomRepository } from 'typeorm'
import { resolve } from 'path'

import { SurveysRepository } from '../repositories/SurveysRepository'
import { SurveysUsersRepository } from '../repositories/SurveysUsersRepository'
import { UsersRepository } from '../repositories/UsersRepository'
import SendMailServices from '../services/SendMailServices'

import { AppError } from '../errors/AppError'

class SendMailController {

  async execute(req: Request, resp: Response) {
    const { email, survey_id } = req.body

    const usersRepository = getCustomRepository(UsersRepository)
    const surveysRepository = getCustomRepository(SurveysRepository)
    const surveysUsersRepository = getCustomRepository(SurveysUsersRepository)
  
    const user = await usersRepository.findOne({ email })
    if(!user) throw new AppError("User does not exists!")

    const survey = await surveysRepository.findOne({ id: survey_id })
    if(!survey) throw new AppError("Survey does not exists!")

    const npsPath = resolve(__dirname, "..", "views", "emails", "npsMail.hbs" )

    const surveyUserAlreadyExists = await surveysUsersRepository.findOne({
      where: { user_id: user.id, value: null },
      // `
      //   SELECT * FROM surveys_users
      //   WHERE value is null
      //   AND user_id = ${user.id}
      // `
      relations: ["user", "survey"]
    })

    const variables = {
      name: user.name,
      title: survey.title,
      description: survey.description,
      id: surveyUserAlreadyExists && surveyUserAlreadyExists.id,
      link: process.env.URL_MAIL,
    }

    if(surveyUserAlreadyExists) {
      await SendMailServices.execute(email, survey.title, variables, npsPath)
      return resp.json(surveyUserAlreadyExists)
    }

    const surveyUser = surveysUsersRepository.create({
      user_id: user.id,
      survey_id,
    })

    await surveysUsersRepository.save(surveyUser)
    variables.id = surveyUser.id

    await SendMailServices.execute(
      email,
      survey.title,
      variables,
      npsPath,
    )

    return resp.status(202).json(surveyUser)

  } 

}

export { SendMailController }